﻿using UnityEngine;
using System.Collections;

public class CheapCameraFollow : MonoBehaviour
{
    public Transform target;

    // Update is called once per frame
    void Update()
    {
        Camera.main.transform.position = new Vector3(target.position.x, target.position.y, -10);
    }
}
