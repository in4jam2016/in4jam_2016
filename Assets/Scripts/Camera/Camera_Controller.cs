﻿using UnityEngine;
using System.Collections;

public class Camera_Controller : MonoBehaviour
{

    public Vector3 root = new Vector3();
    private RoomData curRoom;
    public float speed;
    public float targetOrthoSize;
    private GameObject player;
    public float PlayerOffsetFactor = 3;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 m = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 p = player.transform.position;
        Vector3 target = (root + m + p + p) / 4;
        target.z = -10;
        this.transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * speed);
    }

    public void changeOrthoSize(float size)
    {
        targetOrthoSize = size;
    }

}
