﻿using UnityEngine;

public class FootstepAudio : Audio {

    public float lowPitchRange = .95f;
    public float highPitchRange = 1.05f;

    public override void PlaySingle()
    {
        if (!audioSource.isPlaying) {
            audioSource.pitch = Random.Range(lowPitchRange, highPitchRange);
            audioSource.Play();
        }
    }
}