﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerStats : MonoBehaviour
{

    private float health = 100;
    private float maxHealth = 100;

    public Text healthUI;

    public float Health
    {
        get
        {
            return health;
        }

        set
        {
            health = value;
            healthUI.text = health + "/" + maxHealth;
        }
    }

    public float MaxHealth
    {
        get
        {
            return maxHealth;
        }

        set
        {
            maxHealth = value;
            healthUI.text = health + "/" + maxHealth;
        }
    }

    void Update()
    {
        if(health <= 0)
        {
            Debug.Log("Game Over");
            Application.LoadLevel(Application.loadedLevel);
        }
    }


}