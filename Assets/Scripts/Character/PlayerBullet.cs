﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.collider.tag !=  "Player")
        Destroy(gameObject);
    }

}
