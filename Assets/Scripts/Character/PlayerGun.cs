﻿using UnityEngine;
using System.Collections;

public class PlayerGun : MonoBehaviour {

    public Transform projectile;
    public int currentAmmo;
    public int maxAmmo;
    public float projectileVelocity = 100;

    public Transform shootPos;
    public LineRenderer leftLine;
    public LineRenderer rightLine;
    
    public float focusTime = 1.5f;
    private float aimTime = 0;
    
    private Vector2 leftPoint;
    private Vector2 rightPoint;
    private Quaternion baseRot;

    Vector3 mousePos;

    private Vector2 shootDir;

	// Use this for initialization
	void Start () {
        aimTime = focusTime;

    }
	
	// Update is called once per frame
	void Update () {
        mousePos = Input.mousePosition;
        mousePos.z = 0f;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        shootDir = mousePos - leftLine.transform.position;

        if (Input.GetButtonDown("Fire2"))
        {
            baseRot = Quaternion.LookRotation(transform.forward, shootDir);
            leftLine.transform.rotation = baseRot * Quaternion.AngleAxis(-90, transform.forward);
            rightLine.transform.rotation = baseRot * Quaternion.AngleAxis(90, transform.forward);
            leftLine.enabled = true;
            rightLine.enabled = true;
        }
        if (Input.GetButton("Fire2") && currentAmmo > 0)
        {
            Aim();
        }
        if (Input.GetButtonUp("Fire2") && currentAmmo > 0)
        {
            Shoot();
            aimTime = focusTime;
            leftLine.enabled = false;
            rightLine.enabled = false;
        }
    }

    void Shoot()
    {
        Vector2 actualShootDir = Vector2.zero;

        actualShootDir = Vector3.Slerp(leftLine.transform.up, rightLine.transform.up, Random.value);

        if (actualShootDir.normalized != Vector2.zero)
            actualShootDir.Normalize();

        Transform bullet = (Transform)Instantiate(projectile, shootPos.position, Quaternion.LookRotation(transform.forward, actualShootDir));
        bullet.GetComponent<Rigidbody2D>().AddForce(actualShootDir * projectileVelocity);
        currentAmmo--;
    }

    void Aim()
    {
        if (aimTime > 0)
        {
            aimTime -= Time.deltaTime/2;
            leftLine.transform.rotation = Quaternion.Slerp(leftLine.transform.rotation, baseRot, 1 - (aimTime / focusTime));
            rightLine.transform.rotation = Quaternion.Slerp(rightLine.transform.rotation, baseRot, 1 - (aimTime / focusTime));
        }
    }


}
