﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class PlayerMovement : MonoBehaviour
{
    public RoomData CurRoom;

    private Rigidbody2D rb;
    private FootstepAudio audSrc;

    private float maxVelocity = 5f;
    bool flip = false;
    public bool isMoving = false;

    private float rollTime = .5f;
    private float rollTimer = 0;
    public bool rolling = false;
    private Vector2 direction = new Vector2();
    private float velocity;

    public enum RollState
    {
        Ready,
        Rolling,
        Cooldown
    }
    public RollState rollState;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        audSrc = GameObject.Find("Footstep").GetComponent<FootstepAudio>();
    }

    // Update is called once per frame
    void Update()
    {
        //TODO Break this out into generic class for movement
        //Get input from axes
        float x_input = Input.GetAxisRaw("Horizontal");
        float y_input = Input.GetAxisRaw("Vertical");


        isMoving = x_input != 0 || y_input != 0;

        //Flip sprite based on direction
        if (rollState != RollState.Rolling)
        {
            if (x_input < 0) flip = true;
            else if (x_input > 0) flip = false;
        }

        //Apply input to velocity
        switch (rollState)
        {
            case RollState.Ready:
                bool isDashKeyDown = Input.GetKeyDown(KeyCode.LeftShift);
                direction = new Vector2(x_input, y_input);
                direction.Normalize();
                velocity = maxVelocity;
                if (isDashKeyDown && isMoving && rollTimer == 0)
                {
                    velocity *= 2f;
                    rollState = RollState.Rolling;
                    GetComponent<Animator>().SetBool("dashing", true);
                }
                break;

            case RollState.Rolling:
                rollTimer += Time.deltaTime;
                isMoving = true;
                // frame 4 of 6 (66%) is when the character gets up in the rolling animation
                if (rollTimer > rollTime * .66f)
                {
                    velocity = maxVelocity / 2f;
                }
                if (rollTimer > rollTime)
                {
                    rollTimer = rollTime * 1.1f;
                    rollState = RollState.Cooldown;
                    GetComponent<Animator>().SetBool("dashing", false);
                }
                break;

            case RollState.Cooldown:
                rollTimer -= Time.deltaTime;
                if (rollTimer <= rollTime)
                {
                    rollTimer = 0;
                    rollState = RollState.Ready;
                }
                break;

        }
        rb.velocity = velocity * direction;
        SpriteRenderer renderer = this.GetComponent<SpriteRenderer>();
        renderer.flipX = flip;
        renderer.sortingOrder = Mathf.RoundToInt(CurRoom.RoomRef.transform.position.y + transform.position.y * 100f) * -1;
        string s = Regex.Replace(renderer.sprite.ToString(), "[^0-9]", "");
        if (s == "1" || s == "5") audSrc.PlaySingle();
        this.GetComponent<Animator>().SetBool("moving", isMoving);
    }

}



