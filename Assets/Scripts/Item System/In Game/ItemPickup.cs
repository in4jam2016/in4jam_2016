﻿using UnityEngine;
using System.Collections;

public class ItemPickup : MonoBehaviour {

    public Item itemForPickup;

    public Sprite[] WeaponSprites;
    public Sprite[] ConsumableSprites;

    public WeaponAttribute[] WeaponAttributes;

    // Use this for initialization
    void Start () {
        itemForPickup = GenerateItem();
        this.GetComponent<SpriteRenderer>().sprite = itemForPickup.Sprite;
	}
	
    Item GenerateItem()
    {
        Item item = null;
        float random = Random.value;
        bool isWeapon = false;
        if(random < 0.5f)
        {
            isWeapon = true;
        }
        if (isWeapon)
        {
            item = new Weapon();
            item.Sprite = WeaponSprites[Random.Range(0,WeaponSprites.Length)];
            item.Icon = item.Sprite;
            item.Name = "Weapon";
            item.ItemAttributes.Add(WeaponAttributes[Random.Range(0, WeaponAttributes.Length)]);
            
        }
        else
        {
            item = new Consumable();
            item.Sprite = ConsumableSprites[Random.Range(0, ConsumableSprites.Length)];
            item.Icon = item.Sprite;
            item.Name = "Consumable";
        }

        return item;

    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            col.GetComponent<PlayerInventory>().AddItem(itemForPickup, transform.position);
            Destroy(this.gameObject);
        }
    }
}
