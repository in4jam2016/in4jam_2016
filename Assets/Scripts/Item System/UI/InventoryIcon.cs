﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InventoryIcon : MonoBehaviour {

    public Transform Icon;
    private Item item;
    private float timer = 0;
    private RectTransform rectTransform;
    private RectTransform iconRectTransform;
    // Use this for initialization
    void Start () {
        Icon.parent = transform.parent.parent;

        iconRectTransform = Icon.GetComponent<RectTransform>();
        rectTransform = GetComponent<RectTransform>();

        Vector2 WorldPosition = item.startPosition;
        RectTransform CanvasRect = FindObjectOfType<Canvas>().GetComponent<RectTransform>();
        //calculate the position of the UI element
        //0,0 for the canvas is at the center of the screen, whereas WorldToViewPortPoint treats the lower left corner as 0,0. Because of this, you need to subtract the height / width of the canvas * 0.5 to get the correct position.
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(WorldPosition);
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

        iconRectTransform.anchoredPosition = WorldObject_ScreenPosition;
        
        
	}

    void Update()
    {
        if (timer <= 0.5f)
        {
            timer += Time.deltaTime;
            Icon.position = Vector3.Lerp(Icon.position, transform.position, Time.deltaTime * 10);
        }
        else
        {
            iconRectTransform.position = Vector2.Lerp(iconRectTransform.position, rectTransform.position, Time.deltaTime * 50);
            iconRectTransform.sizeDelta = Vector2.Lerp(iconRectTransform.sizeDelta, rectTransform.sizeDelta, Time.deltaTime * 50);
        }
        
    }

    public Item getItem()
    {
        return item;
    }
    public void setItem(Item newItem)
    {
        item = newItem;
        Icon.GetComponent<Image>().sprite = item.Icon;
    }

    void OnDestroy()
    {
        Destroy(Icon.gameObject);
    }
}
