﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class InventoryUI : MonoBehaviour {
    public PlayerInventory Inventory;
    public List<Item> currentInventory = new List<Item>();
    public Transform iconPrefab;
    float ScrollInput;
	// Use this for initialization
	void Start () {

        RefreshInventory();


    }

    // Update is called once per frame
    void Update()
    {

        ScrollInput = Input.GetAxis("Mouse ScrollWheel");
        if (Inventory.InventoryItems.Count > 0)
        {
            if (ScrollInput > 0)
            {
                transform.GetChild(0).SetAsLastSibling();
                Inventory.currentlyEquipped = transform.GetChild(0).GetComponent<InventoryIcon>().getItem();
                for (int x = 0; x < transform.childCount; x++)
                {
                    if (x != 0)
                    {
                        transform.GetChild(x).GetComponent<LayoutElement>().preferredHeight = 30;
                        transform.GetChild(x).GetComponent<LayoutElement>().preferredWidth = 30;
                    }
                    else
                    {
                        transform.GetChild(x).GetComponent<LayoutElement>().preferredHeight = 50;
                        transform.GetChild(x).GetComponent<LayoutElement>().preferredWidth = 50;
                    }
                }
            }
            else if (ScrollInput < 0)
            {

                transform.GetChild(transform.childCount - 1).SetAsFirstSibling();
                Inventory.currentlyEquipped = transform.GetChild(0).GetComponent<InventoryIcon>().getItem();
                for (int x = 0; x < transform.childCount; x++)
                {
                    if (x != 0)
                    {
                        transform.GetChild(x).GetComponent<LayoutElement>().preferredHeight = 30;
                        transform.GetChild(x).GetComponent<LayoutElement>().preferredWidth = 30;
                    }
                    else
                    {
                        transform.GetChild(x).GetComponent<LayoutElement>().preferredHeight = 50;
                        transform.GetChild(x).GetComponent<LayoutElement>().preferredWidth = 50;
                    }
                }
            }
        }
    }


    public void RefreshInventory()
    {
        foreach(Transform child in transform)
        {
            if (!Inventory.InventoryItems.Contains(child.GetComponent<InventoryIcon>().getItem()))
            {
                Destroy(child);
            }
        }
        foreach (Item i in Inventory.InventoryItems)
        {
            if (!currentInventory.Contains(i))
            {
                Transform icon = (Transform)Instantiate(iconPrefab, this.transform);
                icon.GetComponent<InventoryIcon>().setItem(i);
                currentInventory.Add(icon.GetComponent<InventoryIcon>().getItem());
            }
        }

        if (Inventory.InventoryItems.Count > 0)
        {
            for (int x = 0; x < transform.childCount; x++)
            {
                if (x != 0)
                {
                    transform.GetChild(x).GetComponent<LayoutElement>().preferredHeight = 30;
                    transform.GetChild(x).GetComponent<LayoutElement>().preferredWidth = 30;
                }
                else
                {
                    transform.GetChild(x).GetComponent<LayoutElement>().preferredHeight = 50;
                    transform.GetChild(x).GetComponent<LayoutElement>().preferredWidth = 50;
                }
            }
        }
    }

}
