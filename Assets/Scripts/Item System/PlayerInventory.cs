﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInventory : MonoBehaviour {

    public List<Item> InventoryItems = new List<Item>();
    public InventoryUI UI;
    public Item currentlyEquipped;

    private Audio audSrc;

    void Start()
    {
        audSrc = GameObject.Find("Pickup").GetComponent<Audio>();
    }

    void Update()
    {
        if (currentlyEquipped != null && Input.GetButtonDown("Fire1"))
        {
            currentlyEquipped.use(this.gameObject);
        }
    }

    public void AddItem(Item item, Vector2 position)
    {
        item.startPosition = position;
        InventoryItems.Add(item);
        audSrc.PlaySingle();
        UI.RefreshInventory();
    }

    public Item RemoveItem(int index)
    {
        Item itemToRemove = InventoryItems[index];
        InventoryItems.RemoveAt(index);
        UI.RefreshInventory();
        return itemToRemove;
    }
}
