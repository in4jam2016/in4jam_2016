﻿using UnityEngine;
using System.Collections;
using System;
[CreateAssetMenu(fileName ="DamageAsset",menuName ="Item System/Effects/Damage", order = 0)]
public class DamageEffect : AttackEffect {

    public Transform slashEffect;

    public override bool ApplyEffect(GameObject applyFrom)
    {
        Debug.Log("Attack");
        return true;
    }
}
