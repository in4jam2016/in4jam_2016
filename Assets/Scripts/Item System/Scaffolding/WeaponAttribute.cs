﻿using UnityEngine;
using System.Collections;
[CreateAssetMenu(fileName = "WeaponAttribute",menuName = "Item System/New WeaponAttribute", order = 1)]
public class WeaponAttribute : ItemAttribute {

    public string Name;
    public string Magnitude;

    public AttackEffect[] AttackEffects;

	public override void ApplyAttribute(GameObject applyFrom)
    {
        foreach(AttackEffect effect in AttackEffects)
        {
            effect.ApplyEffect(applyFrom);
        }
    }
}
