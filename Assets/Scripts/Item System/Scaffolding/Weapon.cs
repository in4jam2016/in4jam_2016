﻿using UnityEngine;
using System.Collections;
using System;

public class Weapon : Item
{
    public override void use(GameObject user)
    {
        foreach(ItemAttribute attribute in ItemAttributes)
        {
            attribute.ApplyAttribute(user);
        }
    }
}
