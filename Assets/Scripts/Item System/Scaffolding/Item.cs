﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Item{

    public string Name;
    public Sprite Sprite;
    public Sprite Icon;
    public Vector2 startPosition;
    public List<ItemAttribute> ItemAttributes = new List<ItemAttribute>();

    public abstract void use(GameObject user);


}
