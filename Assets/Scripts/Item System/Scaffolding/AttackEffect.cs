﻿using UnityEngine;
using System.Collections;
public abstract class AttackEffect : ScriptableObject {
    //define an effect that returns a boolean when done
    public abstract bool ApplyEffect(GameObject applyFrom);

}
