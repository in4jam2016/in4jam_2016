﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public abstract class ItemAttribute:ScriptableObject {

    public abstract void ApplyAttribute(GameObject applyFrom);
	
}
