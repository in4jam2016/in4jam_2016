﻿using UnityEngine;
using System.Collections;

public class BurnVictim : MonoBehaviour {
    public float movementSpeed = 5.0f;
    private Rigidbody2D rb;
    private Rigidbody2D fireRb;
    public Vector2 velocity;
    public Vector2 maxVelocity = new Vector2(0.25f, 0.25f);
    public float friction = 0.5f;
    public float distance = 1.0f;
    public Vector3 playerPos;
    public float deltaTime;
    public float fireDelta;
    public float fireRate;
    public Transform fireBallPrefab;
    public float fireSpeed;
    public bool canMove;
    public float animationLock;




    // Use this for initialization
    void Start () {
        deltaTime = 0.0f;
        fireDelta = 0.0f;
        rb = GetComponent<Rigidbody2D>();
        fireRate = 4.0f;
        fireSpeed = 2.0f;
        animationLock = 1.0f;
        canMove = true;

        
    }
	
	// Update is called once per frame
	void Update () {

        deltaTime += Time.deltaTime;
        fireDelta += Time.deltaTime;
        playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;

        //fire bullet if its been 'fireRate' seconds
         
        if (fireRate < deltaTime)
        {
            FireProjectile();
            canMove = false;
            deltaTime = 0;
        }

        if (animationLock > deltaTime)
        {
        
        }
       

        //if he is further than distance units away, move closer
        if (Vector3.Distance(this.transform.position, playerPos) > distance) {

            Vector2 deltaMove = new Vector2();

            Vector2 moveVector = playerPos - transform.position;

            deltaMove = moveVector * movementSpeed;
            rb.velocity = deltaMove;
        }    //else, he is closer than distance, so move away
        else {
            Vector2 deltaMove = new Vector2();

            Vector2 moveVector = playerPos - transform.position;
            moveVector = -moveVector;

            deltaMove = moveVector * movementSpeed * 1.5f;
            rb.velocity = deltaMove;
        }
        



    }

    void FireProjectile()
    {
        Debug.Log("pew");
        Transform fire = (Transform)Instantiate(fireBallPrefab, this.transform.position + Vector3.up, Quaternion.identity);
        //clean this
        Vector2 deltaMove = new Vector2();
        Vector2 moveVector = playerPos - fire.transform.position;
        fireRb = fire.GetComponent<Rigidbody2D>();
        deltaMove = moveVector * fireSpeed;
        fireRb.velocity = deltaMove;
        //fire.GetComponent.transform
    }
}
