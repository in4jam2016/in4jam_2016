﻿using UnityEngine;
using System.Collections;

public class Generic_Enemy : MonoBehaviour {

    //This script should create each prefab(body parts) and attach it to this gameobject as a child. Each part will handle stat generation based on the part.

    public GameObject head,body,legs;
    public float health;
    public float speed;
    public float damage;
    public bool ranged;
    public bool regen;
    bool isAlive;
    // Use this for initialization
    void Start()
    {
        body = GameObject.Find("Body");
        head = GameObject.Find("Head");
        legs = GameObject.Find("Legs");

        body.transform.SetParent(this.transform);
        head.transform.SetParent(this.transform);
        legs.transform.SetParent(this.transform);


    }
	// Update is called once per frame
	void Update () {
	
	}
}
