﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour
{
    public DoorData Data;
    public Sprite OpenSprite;
    public Sprite ClosedSprite;
    public Room ParentRoom;
    private bool isOpen;
    public bool IsOpen
    {
        get
        {
            return isOpen;
        }

        set
        {
            isOpen = value;
            GetComponent<SpriteRenderer>().sprite = value ? OpenSprite : ClosedSprite;
            
        }
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player" && IsOpen)
        {
            col.GetComponent<PlayerMovement>().CurRoom.RoomRef.GetComponent<RoomEffect>().CurrentlyInRoom = false;
            col.GetComponent<PlayerMovement>().CurRoom = ParentRoom.Data;
            ParentRoom.GetComponent<RoomEffect>().CurrentlyInRoom = true;
            Vector3 offset = new Vector3(ParentRoom.Data.Width / 2, ParentRoom.Data.Height / 2);
            Camera.main.GetComponent<Camera_Controller>().root = ParentRoom.transform.position + offset;
        }
    }

    void Start()
    {
        ParentRoom = transform.parent.GetComponent<Room>();
        GetComponent<BoxCollider2D>().isTrigger = isOpen;
    }
}
