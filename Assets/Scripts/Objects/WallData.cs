﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class WallData
{
    public int TypeId;
    public int SpriteId;
    public int OrientationId;
    public int X;
    public int Y;

    public WallData(int spriteid, int orientationid, int x, int y)
    {
        TypeId = RoomGenerator.WallTypeId;
        SpriteId = spriteid;
        OrientationId = orientationid;
        X = x;
        Y = y;
    }
}
