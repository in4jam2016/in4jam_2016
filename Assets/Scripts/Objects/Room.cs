﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Room : MonoBehaviour
{
    public RoomData Data;

    public bool HasDoorLeft = true;
    public bool HasDoorRight = true;
    public bool HasDoorUp = true;
    public bool HasDoorDown = true;

    public List<Transform> DoorObjects = new List<Transform>();

    public void Load (RoomGenerator gen, float roomY)
    {
        LoadFloor(gen, roomY);
        LoadDoors(gen, roomY);
        LoadWalls(gen, roomY);
        //Ignoring everything else for now. Just want to render a room :)
    }

    private void LoadWalls(RoomGenerator gen, float roomY)
    {
        for (int y = 0; y < Data.Height; y++)
        {
            for (int x = 0; x < Data.Width; x++)
            {
                WallData cur = Data.Walls[y * Data.Width + x];
                if (cur == null)
                {
                    continue;
                }
                switch (cur.OrientationId)
                {
                    case RoomGenerator.HorizontalOrientationId:
                        Transform hborder = Instantiate(gen.HorizontalWallTiles[cur.SpriteId]);
                        hborder.name = "Horizontal Wall " + cur.X + ", " + cur.Y;
                        hborder.SetParent(transform);
                        hborder.position = new Vector3(cur.X, cur.Y, 0);
                        hborder.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + hborder.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.EOrientationId:
                        Transform eborder = Instantiate(gen.EWallTiles[cur.SpriteId]);
                        eborder.name = "E Wall " + cur.X + ", " + cur.Y;
                        eborder.SetParent(transform);
                        eborder.position = new Vector3(cur.X, cur.Y, 0);
                        eborder.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + eborder.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.WOrientationId:
                        Transform wborder = Instantiate(gen.WWallTiles[cur.SpriteId]);
                        wborder.name = "W Wall " + cur.X + ", " + cur.Y;
                        wborder.SetParent(transform);
                        wborder.position = new Vector3(cur.X, cur.Y, 0);
                        wborder.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + wborder.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.NECornerOrientationId:
                        Transform necorner = Instantiate(gen.NECornerWallTiles[cur.SpriteId]);
                        necorner.name = "NECorner " + cur.X + ", " + cur.Y;
                        necorner.SetParent(transform);
                        necorner.position = new Vector3(cur.X, cur.Y, 0);
                        necorner.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + necorner.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.NWCornerOrientationId:
                        Transform nwcorner = Instantiate(gen.NWCornerWallTiles[cur.SpriteId]);
                        nwcorner.name = "NWCorner " + cur.X + ", " + cur.Y;
                        nwcorner.SetParent(transform);
                        nwcorner.position = new Vector3(cur.X, cur.Y, 0);
                        nwcorner.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + nwcorner.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.SWCornerOrientationId:
                        Transform swcorner = Instantiate(gen.SWCornerWallTiles[cur.SpriteId]);
                        swcorner.name = "SWCorner " + cur.X + ", " + cur.Y;
                        swcorner.SetParent(transform);
                        swcorner.position = new Vector3(cur.X, cur.Y, 0);
                        swcorner.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + swcorner.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.SECornerOrientationId:
                        Transform secorner = Instantiate(gen.SECornerWallTiles[cur.SpriteId]);
                        secorner.name = "SECorner " + cur.X + ", " + cur.Y;
                        secorner.SetParent(transform);
                        secorner.position = new Vector3(cur.X, cur.Y, 0);
                        secorner.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + secorner.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.SingleOrientationId:
                        Transform single = Instantiate(gen.SingleWallTiles[cur.SpriteId]);
                        single.name = "Single " + cur.X + ", " + cur.Y;
                        single.SetParent(transform);
                        single.position = new Vector3(cur.X, cur.Y, 0);
                        single.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + single.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.QuadOrientationId:
                        Transform quad = Instantiate(gen.QuadWallTiles[cur.SpriteId]);
                        quad.name = "Quad " + cur.X + ", " + cur.Y;
                        quad.SetParent(transform);
                        quad.position = new Vector3(cur.X, cur.Y, 0);
                        quad.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + quad.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.NTripleOrientationId:
                        Transform ntwall = Instantiate(gen.NTripleWallTiles[cur.SpriteId]);
                        ntwall.name = "N Triple Wall " + cur.X + ", " + cur.Y;
                        ntwall.SetParent(transform);
                        ntwall.position = new Vector3(cur.X, cur.Y, 0);
                        ntwall.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + ntwall.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.STripleOrientationId:
                        Transform stwall = Instantiate(gen.STripleWallTiles[cur.SpriteId]);
                        stwall.name = "S Triple Wall " + cur.X + ", " + cur.Y;
                        stwall.SetParent(transform);
                        stwall.position = new Vector3(cur.X, cur.Y, 0);
                        stwall.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + stwall.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.ETripleOrientationId:
                        Transform etwall = Instantiate(gen.ETripleWallTiles[cur.SpriteId]);
                        etwall.name = "E Triple Wall " + cur.X + ", " + cur.Y;
                        etwall.SetParent(transform);
                        etwall.position = new Vector3(cur.X, cur.Y, 0);
                        etwall.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + etwall.position.y) * 100f) * -1;
                        break;
                    case RoomGenerator.WTripleOrientationId:
                        Transform wtwall = Instantiate(gen.WTripleWallTiles[cur.SpriteId]);
                        wtwall.name = "W Triple Wall " + cur.X + ", " + cur.Y;
                        wtwall.SetParent(transform);
                        wtwall.position = new Vector3(cur.X, cur.Y, 0);
                        wtwall.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + wtwall.position.y) * 100f) * -1;
                        break;
                }
            }
        }
    }

    private void LoadDoors (RoomGenerator gen, float roomY)
    {
        for (int i = 0; i < Data.Doors.Length; i++)
        {
            DoorData cur = Data.Doors[i];
            switch (cur.OrientationId)
            {
                case RoomGenerator.NOrientationId:
                    Transform topdoor = Instantiate(gen.DoorTiles[RoomGenerator.TopDoorIndex]);
                    DoorObjects.Add(topdoor);
                    topdoor.gameObject.GetComponent<Door>().Data = Data.Doors[RoomGenerator.TopDoorIndex];
                    topdoor.name = "TopDoor " + cur.X + ", " + cur.Y;
                    topdoor.SetParent(transform);
                    topdoor.position = new Vector3(cur.X, cur.Y, 0);
                    topdoor.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + topdoor.position.y) * 100f) * -1;
                    break;
                case RoomGenerator.WOrientationId:
                    Transform ldoor = Instantiate(gen.DoorTiles[RoomGenerator.LeftDoorIndex]);
                    DoorObjects.Add(ldoor);
                    ldoor.gameObject.GetComponent<Door>().Data = Data.Doors[RoomGenerator.LeftDoorIndex];
                    ldoor.name = "LeftDoor " + cur.X + ", " + cur.Y;
                    ldoor.SetParent(transform);
                    ldoor.position = new Vector3(cur.X, cur.Y, 0);
                    ldoor.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + ldoor.position.y) * 100f) * -1;
                    break;
                case RoomGenerator.EOrientationId:
                    Transform rdoor = Instantiate(gen.DoorTiles[RoomGenerator.RightDoorIndex]);
                    DoorObjects.Add(rdoor);
                    rdoor.gameObject.GetComponent<Door>().Data = Data.Doors[RoomGenerator.RightDoorIndex];
                    rdoor.name = "RightDoor " + cur.X + ", " + cur.Y;
                    rdoor.SetParent(transform);
                    rdoor.position = new Vector3(cur.X, cur.Y, 0);
                    rdoor.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + rdoor.position.y) * 100f) * -1;
                    break;
                case RoomGenerator.SOrientationId:
                    Transform bdoor = Instantiate(gen.DoorTiles[RoomGenerator.BottomDoorIndex]);
                    DoorObjects.Add(bdoor);
                    bdoor.gameObject.GetComponent<Door>().Data = Data.Doors[RoomGenerator.BottomDoorIndex];
                    bdoor.name = "BottomDoor " + cur.X + ", " + cur.Y;
                    bdoor.SetParent(transform);
                    bdoor.position = new Vector3(cur.X, cur.Y, 0);
                    bdoor.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + bdoor.position.y) * 100f) * -1;
                    break;
            }
        }
    }

    private void LoadFloor(RoomGenerator gen, float roomY)
    {
        for (int y = 0; y < Data.Height; y++)
        {
            for (int x = 0; x < Data.Width; x++)
            {
                FloorData cur = Data.FloorTiles[y * Data.Width + x];
                //TODO Handle orientations
                Transform floor = Instantiate(gen.FloorTiles[cur.SpriteId]);
                floor.name = "Floor " + cur.X + ", " + cur.Y;
                floor.SetParent(transform);
                floor.position = new Vector3(cur.X, cur.Y, 0);
                floor.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((roomY + floor.position.y) * 100f) * -1;
            }
        }
    }

    public void DrawDoors()
    {
        foreach (Transform DoorObj in DoorObjects)
        {
            Door door = DoorObj.GetComponent<Door>();
            switch (door.Data.OrientationId)
            {
                case RoomGenerator.NOrientationId:
                    door.IsOpen = !HasDoorUp;
                    door.ParentRoom = this;
                    break;
                case RoomGenerator.EOrientationId:
                    door.IsOpen = !HasDoorRight;
                    door.ParentRoom = this;
                    break;
                case RoomGenerator.SOrientationId:
                    door.IsOpen = !HasDoorDown;
                    door.ParentRoom = this;
                    break;
                case RoomGenerator.WOrientationId:
                    door.IsOpen = !HasDoorLeft;
                    door.ParentRoom = this;
                    break;
            }
        }
    }
}
