﻿using UnityEngine;
using System.Collections;

public class PuzzleData
{
    public int Id;
    public int X;
    public int Y;

    public PuzzleData(int id, int x, int y)
    {
        Id = id;
        X = x;
        Y = y;
    }
}
