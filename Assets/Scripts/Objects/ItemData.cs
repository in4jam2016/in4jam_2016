﻿using UnityEngine;
using System.Collections;

public class ItemData
{
    public int Id;
    public int X;
    public int Y;

    public ItemData(int id, int x, int y)
    {
        Id = id;
        X = x;
        Y = y;
    }
}
