﻿using UnityEngine;
using System.Collections.Generic;

public enum RoomType
{
    Normal, Boss, Item
}
[System.Serializable]
public class RoomData
{
    public Room RoomRef;
    public Vector2 Position;
    public int Width;   //measured in cells
    public int Height;  //measured in cells
    public int Level;
    public RoomType Type;
    public FloorData[] FloorTiles;
    public WallData[] Walls;
    public List<EnemyData> Enemies;
    public List<PuzzleData> Puzzles;
    public List<ItemData> Items;
    public DoorData[] Doors;

    public RoomData(int level, int width, int height, RoomType type)
    {
        Level = level;
        Type = type;
        Width = width;
        Height = height;
        Doors = new DoorData[4];
        Walls = new WallData[width * height];
        FloorTiles = new FloorData[width * height];

        if (type == RoomType.Normal)
        {
            int dir = Random.Range(-1, 2);
            int enemyAve = (int)Mathf.Sqrt(1f / .002f + Mathf.Pow(2f, -Level));
            int puzzleAve = (int)Mathf.Sqrt(1f / .009f + Mathf.Pow(1.63f, -Level));
            int enemyStDev = Random.Range(Mathf.FloorToInt(enemyAve * .15f), Mathf.FloorToInt(enemyAve * .5f));
            int puzzleStDev = Random.Range(Mathf.FloorToInt(puzzleAve * .15f), Mathf.FloorToInt(puzzleAve * .5f));
            Enemies = new List<EnemyData>(enemyAve + (enemyStDev * dir));
            Puzzles = new List<PuzzleData>(puzzleAve + (puzzleStDev * dir));
        }
        else if (type == RoomType.Boss)
        {
            Enemies = new List<EnemyData>(1);
        }
        else
        {
            Items = new List<ItemData>(1);
        }
    }
}
