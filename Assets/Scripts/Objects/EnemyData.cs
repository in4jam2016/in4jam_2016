﻿using UnityEngine;
using System.Collections;

public class EnemyData
{
    public int Id;
    public int X;
    public int Y;

    public EnemyData(int id, int x, int y)
    {
        Id = id;
        X = x;
        Y = y;
    }
}
