﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class DoorData
{
    public int TypeId;
    public int SpriteId;
    public int OrientationId;
    public int X;
    public int Y;
    public readonly RoomData RoomRef;

    public DoorData(int spriteid, int orientationid, int x, int y, RoomData roomRef)
    {
        RoomRef = roomRef;
        TypeId = RoomGenerator.DoorTypeId;
        SpriteId = spriteid;
        OrientationId = orientationid;
        X = x;
        Y = y;
    }
}
