﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class FloorData
{
    public int TypeId;
    public int SpriteId;
    public int X;
    public int Y;
    public bool Blocked;

    public FloorData(int spriteid, int orientationid, int x, int y, bool blocked)
    {
        TypeId = RoomGenerator.FloorTypeId;
        SpriteId = spriteid;
        X = x;
        Y = y;
        Blocked = blocked;
    }
}
