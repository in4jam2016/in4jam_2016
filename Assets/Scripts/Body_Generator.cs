﻿using UnityEngine;
using System.Collections;

public class Body_Generator : MonoBehaviour {
    public float health;
    public float min = 1;
    public float max = 10;
    public bool regen;

    //for now, this class handles hp and hp regen properties.
	// Use this for initialization
	void Start () {
       health = (int)Random.Range(min, max); //will include floor level in calculation later;
        if (Random.Range(min, max) > 7.5f) { regen = true; } //all this to be made later. 
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    //data sharing methods
    float getHealth()
    {
        return health;
    }
    bool isRegen()
    {
        return regen;
    }
}
