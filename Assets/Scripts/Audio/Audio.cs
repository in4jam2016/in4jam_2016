﻿using UnityEngine;

public class Audio : MonoBehaviour {
    public AudioSource audioSource;

    public virtual void PlaySingle() {
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }
}
