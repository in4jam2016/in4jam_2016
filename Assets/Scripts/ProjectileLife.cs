﻿using UnityEngine;
using System.Collections;

public class ProjectileLife : MonoBehaviour {
    float deltaTime;
    public float lifeTime;
	// Use this for initialization
	void Start () {
        lifeTime = 5.0f;
        deltaTime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        deltaTime += Time.deltaTime;
        if(lifeTime < deltaTime)
        {
            Destroy(this.gameObject);
        }
	}
}
