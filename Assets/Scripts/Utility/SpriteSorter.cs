﻿using UnityEngine;
using System.Collections;

public class SpriteSorter : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt(transform.position.y + transform.position.y * 100f) * -1;
    }
}
