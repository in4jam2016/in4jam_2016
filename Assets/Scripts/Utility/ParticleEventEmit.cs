﻿using UnityEngine;
using System.Collections;

public class ParticleEventEmit : MonoBehaviour {

    public ParticleSystem ps;

    public void Emit()
    {
        ps.Emit(10);
    }

}
