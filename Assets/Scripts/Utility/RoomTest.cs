﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomTest : MonoBehaviour {

    public TextAsset roomTextData;
    private int[,,] roomData;
    public Transform[] tiles;
    
	// Use this for initialization
	void Start () {
        //Read the text file into the 2D array
        string[] lines = roomTextData.text.Split("\n"[0]);

        for (int i = 0; i < lines.Length; i++) {
            for (int j = 0; j < lines[i].Length; j++)
            {
                int tileIndex;
                Vector2 offset = new Vector2(j, -i);
                
                if(int.TryParse(lines[i][j].ToString(), out tileIndex))
                    Instantiate(tiles[tileIndex],transform.position + (Vector3)offset,Quaternion.identity, this.transform);
            }
        }

    }
	

}
