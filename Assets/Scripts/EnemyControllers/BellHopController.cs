﻿using UnityEngine;
using System.Collections;

public class BellHopController : MonoBehaviour {
    public float minWait = 3.0f;
    public float maxWait = 5.0f;
    float waitTime;
    public float bellTime;
    public GameObject theBell;
    Renderer bellRender;
    public Vector3 bellOffset = new Vector3(0.3f, -1.5f, 0.0f);
    public float movementSpeed = 20.0f;
    private Rigidbody2D rb;
    public Vector2 velocity;
    public Vector2 maxVelocity = new Vector2(0.25f, 0.25f);
    public float friction = 0.5f;
    public float distance = 1.1f;
    public Vector3 playerPos;
    public float deltaTime;
    state currentState;
    public GameObject Bell;

    public float Hp;

    enum state
    {
        Waiting,  //wait phase
        BellEnable,
        Readied,  //animation for the jump
        Dashing,  //actual dash
        BellDisable,
        Landing,  // impact landing
        TakeDamage,
        Die

    }


    void Start () {
        currentState = state.Waiting;
        deltaTime = 0.0f;
        waitTime = 2.0f;
        rb = GetComponent<Rigidbody2D>();
        Instantiate(Bell, (GameObject.FindGameObjectWithTag("Player").transform.position - bellOffset), this.transform.rotation); //theBell is sorta redundant
        theBell = GameObject.FindGameObjectWithTag("Bell");
        bellRender = theBell.GetComponent<Renderer>();
        bellRender.enabled = false;
    }
	

	void Update () {

        deltaTime += Time.deltaTime;  //iiiiive, had, the time of my liiiiiiiife, and i, owe it all to youuuuuuuuuuuuuuu
        theBell.transform.position = GameObject.FindGameObjectWithTag("Player").transform.position - bellOffset; //Think there may be a better way than the findgame object
        switch (currentState)
        {
            case state.Waiting: //Wait state
                Debug.Log("enter waiting");
                if (deltaTime >= 2.0f && !bellRender.enabled) { currentState = state.BellEnable; break; }//give player 2.0 seconds to respond to the bell
                if (deltaTime >= waitTime)  //if delta passes the waiting time, then its time to dash
                {
                    currentState = state.Readied;
                }
                break;

            case state.BellEnable: //purely to set up bell render
                bellRender.enabled = true;
                currentState = state.Waiting;
                break;

            case state.Readied: //Readied
                GrabDestination();      //bellhop should only dash at player location at time of bell, not follow the player like a homing missile.
                currentState = state.Dashing;
                break;

            case state.Dashing: //Dashing
                if (Vector3.Distance(this.transform.position, playerPos) > distance) //if its time to dash, then dash towards player until at player destination at dash initiation.
                {
                    Debug.Log(Vector3.Distance(this.transform.position, playerPos));
                    Vector2 deltaMove = new Vector2();
                    Vector2 moveVector = playerPos - transform.position;
                    deltaMove += moveVector.normalized * movementSpeed;
                    rb.velocity = deltaMove;
                }
                else
                {
                    Vector2 deltaMove = new Vector2();
                    deltaMove.x = 0.0f;
                    deltaMove.y = 0.0f;
                    rb.velocity = deltaMove;
                    currentState = state.Landing;
                }
                break;

            case state.Landing: //Landing
                waitTime = Random.Range(minWait, maxWait);  //gives random waittime for stand state
                currentState = state.BellDisable;
                break;

            case state.BellDisable:
                bellRender.enabled = false;
                deltaTime = 0;
                currentState = state.Waiting;
                break;
        }
    }


    void GrabDestination()
    {
        playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;

    }
}
