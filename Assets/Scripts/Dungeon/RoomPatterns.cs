﻿using System.Collections.Generic;
using UnityEngine;

public static class RoomPatterns
{
    #region Box Pattern
    /**
    <summary> Minimum is 10 cells wide or tall </summary>
    **/
    public static void BoxPattern (RoomGenerator gen, RoomData room, int width, int height)
    {
        int cx = Random.Range(2, (width / 4) + 1);
        int cy = Random.Range(2, (height / 4) + 1);
        int sx = width - cx - 1;
        int sy = height - cy - 1;
        int extx = Random.Range(0, sx / 4);
        int exty = Random.Range(0, sy / 4);
        int quadchance = Random.Range(0, 100);

        room.Walls[cy * room.Width + cx] = MakeCornerAt(gen, room, cx, cy, extx, exty, RoomGenerator.SWCornerOrientationId);
        room.Walls[cy * room.Width + sx] = MakeCornerAt(gen, room, sx, cy, extx, exty, RoomGenerator.SECornerOrientationId);
        room.Walls[sy * room.Width + cx] = MakeCornerAt(gen, room, cx, sy, extx, exty, RoomGenerator.NWCornerOrientationId);
        room.Walls[sy * room.Width + sx] = MakeCornerAt(gen, room, sx, sy, extx, exty, RoomGenerator.NECornerOrientationId);

        if (quadchance >= Random.Range(0, 100))
        {
            room.Walls[(cy + 1) * room.Width + (cx + 1)] = new WallData(Random.Range(0, gen.SingleWallTiles.Length), RoomGenerator.SingleOrientationId, (cx + 1), (cy + 1));
        }
        if (quadchance >= Random.Range(0, 100))
        {
            room.Walls[(cy + 1) * room.Width + (sx - 1)] = new WallData(Random.Range(0, gen.SingleWallTiles.Length), RoomGenerator.SingleOrientationId, (sx - 1), (cy + 1));
        }
        if (quadchance >= Random.Range(0, 100))
        {
            room.Walls[(sy - 1) * room.Width + (cx + 1)] = new WallData(Random.Range(0, gen.SingleWallTiles.Length), RoomGenerator.SingleOrientationId, (cx + 1), (sy - 1));
        }
        if (quadchance >= Random.Range(0, 100))
        {
            room.Walls[(sy - 1) * room.Width + (sx - 1)] = new WallData(Random.Range(0, gen.SingleWallTiles.Length), RoomGenerator.SingleOrientationId, (sx - 1), (sy - 1));
        }

        for (int x = cx + 1; x <= cx + extx; x++)
        {
            room.Walls[cy * room.Width + x] = new WallData(Random.Range(0, gen.HorizontalWallTiles.Length), RoomGenerator.HorizontalOrientationId, x, cy);
            room.Walls[sy * room.Width + x] = new WallData(Random.Range(0, gen.HorizontalWallTiles.Length), RoomGenerator.HorizontalOrientationId, x, sy);
        }

        for (int x = sx - 1; x >= sx - extx; x--)
        {
            room.Walls[cy * room.Width + x] = new WallData(Random.Range(0, gen.HorizontalWallTiles.Length), RoomGenerator.HorizontalOrientationId, x, cy);
            room.Walls[sy * room.Width + x] = new WallData(Random.Range(0, gen.HorizontalWallTiles.Length), RoomGenerator.HorizontalOrientationId, x, sy);
        }

        for (int y = cy + 1; y <= cy + exty; y++)
        {
            room.Walls[y * room.Width + cx] = new WallData(Random.Range(0, gen.WWallTiles.Length), RoomGenerator.VerticalOrientationId, cx, y);
            room.Walls[y * room.Width + sx] = new WallData(Random.Range(0, gen.EWallTiles.Length), RoomGenerator.VerticalOrientationId, sx, y);
        }

        for (int y = sy - 1; y >= sy - exty; y--)
        {
            room.Walls[y * room.Width + cx] = new WallData(Random.Range(0, gen.WWallTiles.Length), RoomGenerator.VerticalOrientationId, cx, y);
            room.Walls[y * room.Width + sx] = new WallData(Random.Range(0, gen.EWallTiles.Length), RoomGenerator.VerticalOrientationId, sx, y);
        }
    }

    private static WallData MakeCornerAt (RoomGenerator gen, RoomData room, int x, int y, int extx, int exty, int orientationid)
    {
        WallData wall = new WallData(0, 0, x, y);
        if (extx != 0 && exty != 0)
        {
            wall.OrientationId = orientationid;
            switch (orientationid)
            {
                case RoomGenerator.SECornerOrientationId:
                    wall.SpriteId = Random.Range(0, gen.SECornerWallTiles.Length);
                    break;
                case RoomGenerator.SWCornerOrientationId:
                    wall.SpriteId = Random.Range(0, gen.SWCornerWallTiles.Length);
                    break;
                case RoomGenerator.NECornerOrientationId:
                    wall.SpriteId = Random.Range(0, gen.NECornerWallTiles.Length);
                    break;
                case RoomGenerator.NWCornerOrientationId:
                    wall.SpriteId = Random.Range(0, gen.NWCornerWallTiles.Length);
                    break;
            }
        }
        else
        {
            if (exty == 0 && extx != 0)
            {
                wall.OrientationId = RoomGenerator.HorizontalOrientationId;
                wall.SpriteId = Random.Range(0, gen.HorizontalWallTiles.Length);
            }
            else if (extx == 0 && exty != 0)
            {
                if (orientationid == RoomGenerator.NWCornerOrientationId || orientationid == RoomGenerator.SWCornerOrientationId)
                {
                    wall.OrientationId = RoomGenerator.WOrientationId;
                    wall.SpriteId = Random.Range(0, gen.WWallTiles.Length);
                }
                else if (orientationid == RoomGenerator.NECornerOrientationId || orientationid == RoomGenerator.SECornerOrientationId)
                {
                    wall.OrientationId = RoomGenerator.EOrientationId;
                    wall.SpriteId = Random.Range(0, gen.EWallTiles.Length);
                }
            }
            else
            {
                wall.OrientationId = RoomGenerator.SingleOrientationId;
                wall.SpriteId = Random.Range(0, gen.SingleWallTiles.Length);
            }
        }
        return wall;
    }
    #endregion

    #region Maze Pattern
    public static void MazePattern(RoomGenerator gen, RoomData room, int width, int height)
    {
        //Initialize with all walls for Randomized Prim Algorithm 
        for (int y = 2; y < room.Height - 2; y++)
        {
            for (int x = 2; x < room.Width - 2; x++)
            {
                room.Walls[y * room.Width + x] = new WallData(Random.Range(0, gen.HorizontalWallTiles.Length), RoomGenerator.HorizontalOrientationId, x, y);
            }
        }
        
        //Run Randomized Prim excluding the edges (we want them to stay as they are)
        List<WallData> walls = new List<WallData>();
        List<WallData> adj = gen.GetAdjacent(room.Walls[2 * room.Width + (room.Width / 2)], RoomGenerator.WallTypeId, room);
        walls.AddRange(adj);

        while (walls.Count > 0)
        {
            int choice = Random.Range(0, walls.Count);
            WallData cur = walls[choice];
            adj = gen.GetAdjacent(cur, RoomGenerator.WallTypeId, room);
            if (adj.Count == 3)
            {
                walls.AddRange(gen.GetAdjacent(cur, RoomGenerator.WallTypeId, room));
                room.Walls[cur.Y * room.Width + cur.X] = null;
            }
            walls.RemoveAt(choice);
        }

        /* Not done yet!
        for (int y = 2; y < room.Height - 2; y++)
        {
            for (int x = 2; x < room.Width - 2; x++)
            {
                switch (GetSurroundingWallCount(room, x, y))
                {
                    case 0:
                        room.Tiles[y * room.Width + x].OrientationId = RoomGenerator.SingleOrientationId;
                    break;
                }
            }
        }
        */
    }
    #endregion

    #region Cell Pattern
    public static void CellPattern(RoomGenerator gen, RoomData room, int width, int height)
    {
        //fill map with random walls or floors
        // Fill less in center lines of room for cheap way to gaurantee doors are clear

        for (int x = 1; x < width - 1; x++)
        {
            for (int y = 1; y < height - 1; y++)
            {
                if ((y > room.Height / 2 || y < room.Height / 2) && (x > room.Width / 2 || x < room.Width / 2))
                {
                    if (Random.Range(0, 100) < gen.RandomFillPercent)
                    {
                        room.Walls[y * room.Width + x] = new WallData(Random.Range(0, gen.SingleWallTiles.Length), RoomGenerator.SingleOrientationId, x, y);
                    }
                }
            }
        }

        //smooth with cellular automata algorithm
        for (int i = 0; i < gen.SmoothingIterations; i++)
        {
            SmoothMap(gen, room);
        }
    }

    private static int GetSurroundingWallCount(RoomData room, int gridX, int gridY)
    {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (neighbourX >= 0 && neighbourX < room.Width && neighbourY >= 0 && neighbourY < room.Height)
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        if (room.Walls[gridY * room.Width + gridX] != null)
                        {
                            wallCount++;
                        }
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }

    private static void SmoothMap(RoomGenerator gen, RoomData room)
    {
        for (int x = 1; x < room.Width - 1; x++)
        {
            for (int y = 1; y < room.Height - 1; y++)
            {
                int neighbourWallTiles = GetSurroundingWallCount(room, x, y);

                if (neighbourWallTiles > 4)
                {
                    room.Walls[y * room.Width + x] = new WallData(Random.Range(0, gen.SingleWallTiles.Length), RoomGenerator.SingleOrientationId, x, y);
                }
                else if (neighbourWallTiles < 4)
                {
                    room.Walls[y * room.Width + x] = null;
                }
            }
        }
    }
    #endregion
}