﻿using UnityEngine;
using System.Collections;

public class RoomEffect : MonoBehaviour {

    public Material DarkMaterial;
    public Material LightMaterial;

    bool currentlyInRoom = false;


    void Start()
    {
        foreach (Transform child in this.transform)
        {
            child.GetComponent<SpriteRenderer>().material = currentlyInRoom ? LightMaterial : DarkMaterial;
        }
    }

    public bool CurrentlyInRoom
    {
        get
        {
            return currentlyInRoom;
        }

        set
        {
            currentlyInRoom = value;
            if (value == false)
            {
                foreach(Transform child in this.transform)
                {
                    child.GetComponent<SpriteRenderer>().material = DarkMaterial;
                }
            }
            else
            {
                foreach (Transform child in this.transform)
                {
                    child.GetComponent<SpriteRenderer>().material = LightMaterial;
                }
            }

        }
    }
}
