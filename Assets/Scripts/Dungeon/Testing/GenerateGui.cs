﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GenerateGui : MonoBehaviour {

    public RoomManager Manager;
    public RoomGenerator Generator;

	public Slider rmAmount;
	public Slider rmMaxH;
	public Slider rmMinH;
	public Slider rmMaxW;
	public Slider rmMinW;

    public Slider rmFillPercent;
    public Slider rmSmoothingIterations;


    void Update()
    {

    }

	public void Submit(){
        if(rmMaxH.value >= rmMinH.value && rmMaxW.value >= rmMinH.value)
        {
            Manager.MaxRoomHeight = (int)rmMaxH.value;
            Manager.MaxRoomWidth = (int)rmMaxW.value;
            Manager.MinRoomHeight = (int)rmMinH.value;
            Manager.MinRoomWidth = (int)rmMinW.value;

            Generator.RandomFillPercent = (int)rmFillPercent.value;
            Generator.SmoothingIterations = (int)rmSmoothingIterations.value;

            Manager.generateRooms((int)rmAmount.value);
        }
    }

}
