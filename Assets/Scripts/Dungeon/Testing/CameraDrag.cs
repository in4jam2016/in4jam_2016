﻿using UnityEngine;

using System.Collections;

public class CameraDrag : MonoBehaviour {
	public float maxZoom;
	public float minZoom;

	public float zoom;



	private Vector3 v3OrgMouse;
	private Plane plane = new Plane(-Vector3.forward, Vector3.zero);
		
	void Start(){  
		zoom = this.gameObject.GetComponent<Camera>().orthographicSize;
		//set max camera bounds (assumes camera is max zoom and centered on Start)
	}
	
	void Update(){

		
		//zoom
		
		zoom -= Input.GetAxis("Mouse ScrollWheel") * 3;

		zoom = Mathf.Clamp(zoom,minZoom,maxZoom);
		this.gameObject.GetComponent<Camera>().orthographicSize = Mathf.Lerp(this.gameObject.GetComponent<Camera>().orthographicSize,zoom,Time.deltaTime * 10);

		//click and drag
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			float dist;
			plane.Raycast (ray, out dist);
			v3OrgMouse = ray.GetPoint (dist);
			v3OrgMouse.z = -10;
		} 
		else if (Input.GetMouseButton (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			float dist;
			plane.Raycast (ray, out dist);
			Vector3 v3Pos = ray.GetPoint (dist);
			v3Pos.z = -10;            
			transform.position -= (v3Pos - v3OrgMouse);
		}
	}
}