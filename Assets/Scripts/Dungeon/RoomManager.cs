﻿using UnityEngine;using System.Collections;using System.Collections.Generic;
using System.Linq;public class RoomManager : MonoBehaviour
{
    public string Seed = "testing";
    public List<Room> Rooms = new List<Room>();
    public RoomGenerator generator;
    public int Floor = 1;
    public int RoomAmount = 25;
    public Transform RoomPrefab;

    public int MaxRoomHeight = 10;
    public int MaxRoomWidth = 10;
    public int MinRoomHeight = 6;
    public int MinRoomWidth = 6;

    public struct TempRoom
    {
        public string Name;
        public int Width, Height;
        public Vector2 Position;
        public bool DoorUp, DoorDown, DoorLeft, DoorRight;
    }

    // Use this for initialization
    void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.SetActive(false);

        generateRooms(RoomAmount);

        //Random.InitState(Seed.GetHashCode());

        player.GetComponent<PlayerMovement>().CurRoom = Rooms[0].Data;
        player.transform.position = Rooms[0].DoorObjects[RoomGenerator.BottomDoorIndex].position + Vector3.up;
        player.SetActive(true);
        Vector3 offset = new Vector3(Rooms[0].Data.Width / 2, Rooms[0].Data.Height / 2);
        Camera.main.GetComponent<Camera_Controller>().root = Rooms[0].transform.position + offset;
        Rooms[0].GetComponent<RoomEffect>().CurrentlyInRoom = true;
    }

    public void generateRooms(int roomCount)
    {
        for (int i = 0; i < roomCount; i++)
        {
            TempRoom r = new TempRoom();
            r.Height = RandomEven(MinRoomHeight, MaxRoomHeight);
            r.Width = RandomEven(MinRoomWidth, MaxRoomWidth);
            r.Position = Vector2.zero;
            r.Name = "Room " + i;
            r.DoorUp = true;
            r.DoorDown = true;
            r.DoorLeft = true;
            r.DoorRight = true;

            if (i > 0)
            {
                pickRoomPos(ref r, i);
            }

            Transform room = Instantiate(RoomPrefab) as Transform;
            room.name = r.Name;
            RoomData newRoomData = generator.MakeNormalRoom(Floor, r.Width, r.Height, null);
            Room generatedRoom = room.GetComponent<Room>();
            newRoomData.RoomRef = generatedRoom;
            generatedRoom.Data = newRoomData;
            generatedRoom.HasDoorUp = r.DoorUp;
            generatedRoom.HasDoorDown = r.DoorDown;
            generatedRoom.HasDoorLeft = r.DoorLeft;
            generatedRoom.HasDoorRight = r.DoorRight;
            generatedRoom.Load(generator, r.Position.y);
            generatedRoom.DrawDoors();
            room.transform.position = r.Position;

            Rooms.Add(generatedRoom);
        }
    }

    void pickRoomPos(ref TempRoom r, int index)
    {
        Room prevRoom = null;
        string con = "";
        do
        {
            r.Height = RandomEven(MinRoomHeight, MaxRoomHeight);
            r.Width = RandomEven(MinRoomWidth, MaxRoomWidth);

            prevRoom = Rooms[Random.Range(0, Rooms.Count)];
            float rand = Random.value;
            //make door connections and then make the doors unavailable
            if (rand < 0.25f && prevRoom.HasDoorDown)
            {
                con = "down";
            }
            else if (rand >= 0.25f && rand < 0.5f && prevRoom.HasDoorUp)
            {
                con = "up";
            }
            else if (rand >= 0.5f && rand < 0.75f && prevRoom.HasDoorLeft)
            {
                con = "left";
            }
            else
            {
                con = "right";
            }

            switch (con)
            {
                case "down":
                    r.Position = prevRoom.transform.position + new Vector3(+(prevRoom.Data.Width / 2 - r.Width / 2), -(r.Height), 0);
                    break;
                case "up":
                    r.Position = prevRoom.transform.position + new Vector3(+(prevRoom.Data.Width / 2 - r.Width / 2), +(prevRoom.Data.Height), 0);
                    break;
                case "left":
                    r.Position = prevRoom.transform.position + new Vector3(-(r.Width), +(prevRoom.Data.Height / 2 - r.Height / 2), 0);
                    break;
                case "right":
                    r.Position = prevRoom.transform.position + new Vector3(+(prevRoom.Data.Width), +(prevRoom.Data.Height / 2 - r.Height / 2), 0);
                    break;
            }

        } while (CheckPos(r));

        switch (con)
        {
            case "down":
                prevRoom.HasDoorDown = false;
                r.DoorUp = false;
                r.DoorDown = true;
                r.DoorLeft = true;
                r.DoorRight = true;
                break;
            case "up":
                prevRoom.HasDoorUp = false;
                r.DoorDown = false;
                r.DoorUp = true;
                r.DoorLeft = true;
                r.DoorRight = true;
                break;
            case "left":
                prevRoom.HasDoorLeft = false;
                r.DoorRight = false;
                r.DoorUp = true;
                r.DoorDown = true;
                r.DoorLeft = true;
                break;
            case "right":
                prevRoom.HasDoorRight = false;
                r.DoorLeft = false;
                r.DoorUp = true;
                r.DoorDown = true;
                r.DoorRight = true;
                break;
        }

        prevRoom.DrawDoors();
    }

    bool CheckPos(TempRoom r)
    {
        int bad = 0;
        foreach (Room rm in Rooms)
        {

            if (checkOverlap(r, rm))
            {
                bad++;
            }

        }
        if (bad == 0)
        {
            return false;
        }

        return true;

    }

    bool checkOverlap(TempRoom r, Room rm)
    {
        float l1 = r.Position.x;
        float r1 = r.Position.x + r.Width;
        float b1 = r.Position.y;
        float t1 = r.Position.y + r.Height;

        float l2 = rm.transform.position.x;
        float r2 = rm.transform.position.x + rm.Data.Width;
        float b2 = rm.transform.position.y;
        float t2 = rm.transform.position.y + rm.Data.Height;

        if ((r1 <= l2 || l1 >= r2) || (t1 <= b2 || b1 >= t2))
        {
            return false;
        }

        return true;

    }


    public void ClearRooms()
    {
        foreach (Room r in Rooms)
        {
            foreach (Transform g in r.transform)
            {
                Destroy(g.gameObject);
            }
            Destroy(r.gameObject);

        }
        Rooms.Clear();

    }

    int RandomEven(int min, int max)
    {
        int randint = Random.Range(min, max);
        randint = randint % 2 == 0 ? randint : randint - 1;
        return randint;
    }
}