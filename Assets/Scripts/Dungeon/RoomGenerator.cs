﻿using UnityEngine;
using System.Collections.Generic;

public class RoomGenerator : MonoBehaviour
{
    [Range(0,100)]
    public int RandomFillPercent = 25;
    public int SmoothingIterations = 10;
    
    public Transform[] HorizontalWallTiles;
    public Transform[] WWallTiles;
    public Transform[] EWallTiles;
    public Transform[] SECornerWallTiles;
    public Transform[] SWCornerWallTiles;
    public Transform[] NECornerWallTiles;
    public Transform[] NWCornerWallTiles;
    public Transform[] STripleWallTiles;
    public Transform[] NTripleWallTiles;
    public Transform[] ETripleWallTiles;
    public Transform[] WTripleWallTiles;
    public Transform[] QuadWallTiles;
    public Transform[] SingleWallTiles;

    public Transform[] FloorTiles;                  //sprites are 1 tiles tall and 1 tile wide

    public Transform[] DoorTiles;                   //sprites are 2 tiles tall and 1 tile wide

    public Transform[] Bosses;
    public Transform[] Enemies;
    public Transform[] Items;
    public Transform[] Puzzles;

    public const int FloorTypeId    = -11;
    public const int WallTypeId     = -12;
    public const int DoorTypeId     = -13;

    public const int SingleOrientationId        = -1000;
    public const int HorizontalOrientationId    = -1001;
    public const int VerticalOrientationId      = -1002;
    public const int NOrientationId             = -1003;
    public const int SOrientationId             = -1004;
    public const int EOrientationId             = -1005;
    public const int WOrientationId             = -1006;
    public const int NWCornerOrientationId      = -1007;
    public const int NECornerOrientationId      = -1008;
    public const int SWCornerOrientationId      = -1009;
    public const int SECornerOrientationId      = -1010;
    public const int NTripleOrientationId       = -1011;
    public const int STripleOrientationId       = -1012;
    public const int ETripleOrientationId       = -1013;
    public const int WTripleOrientationId       = -1014;
    public const int QuadOrientationId          = -1015;

    public const int TopDoorIndex       = 0;
    public const int LeftDoorIndex      = 1;
    public const int RightDoorIndex     = 2;
    public const int BottomDoorIndex    = 3;
    public static readonly int[] OppositeDoorIndexes = { 3, 2, 1, 0 };

    public System.Action<RoomGenerator, RoomData, int, int>[] Patterns =
    {
         new System.Action<RoomGenerator, RoomData, int, int> ((gen, room, width, height) => RoomPatterns.MazePattern(gen, room, width, height)),
         new System.Action<RoomGenerator, RoomData, int, int> ((gen, room, width, height) => RoomPatterns.CellPattern(gen, room, width, height)),
         new System.Action<RoomGenerator, RoomData, int, int> ((gen, room, width, height) => RoomPatterns.BoxPattern(gen, room, width, height))
    };

    public RoomData MakeNormalRoom(int level, int width, int height, List<int> inventory)
    {
        RoomData room = new RoomData(level, width, height, RoomType.Normal);

        Patterns[Random.Range(0, Patterns.Length)].Invoke(this, room, width, height);

        SetFloor(room);
        SetBorder(room);
        
        //Lastly, place all the puzzle pieces into the room
        MakePuzzle(room);
        
        return room;
    }

    public RoomData MakeItemRoom(int level, int width, int height, List<int> inventory)
    {
        RoomData room = new RoomData(level, width, height, RoomType.Item);

        SetFloor(room);
        SetBorder(room);

        //Place item in room
        room.Items[0] = new ItemData(Random.Range(0, Items.Length), Random.Range(1, room.Width - 1), Random.Range(1, room.Height - 1));

        //TODO Make pattern here
        
        return room;
    }

    public RoomData MakeBossRoom(int level, int width, int height, List<int> inventory)
    {
        RoomData room = new RoomData(level, width, height, RoomType.Boss);

        SetFloor(room);
        SetBorder(room);
        
        //Place boss in room
        room.Enemies[0] = new EnemyData(Random.Range(0, Enemies.Length), Random.Range(1, room.Width - 1), Random.Range(1, room.Height - 1));

        //TODO Make pattern here
        
        return room;
    }

    private void MakePuzzle(RoomData room)
    {
        int nextPuzzleIndex = 0;
        //Find a starting location
        for (int y = 0; y < room.Height; y++)
        {
            for (int x = 0; x < room.Width; x++)
            {
                if (room.FloorTiles[y * room.Width + x].Blocked == false)
                {
                    if (nextPuzzleIndex < room.Puzzles.Count)
                    {
                        room.Puzzles[nextPuzzleIndex] = new PuzzleData(Random.Range(0, Puzzles.Length), x, y);
                        nextPuzzleIndex++;
                        int dist = Random.Range(1, 6);
                        int c = 0;
                        Stack<Vector2> stack = new Stack<Vector2>();
                        stack.Push(new Vector2(x, y));
                        while (stack.Count > 0)
                        {
                            Vector2 cur = stack.Pop();
                            int cy = (int)cur.y;
                            int cx = (int)cur.x;
                            room.FloorTiles[cy * room.Width + cx].Blocked = true;
                            c++;
                            if (c == dist)
                            {
                                dist = Random.Range(1, 6);
                                //Get previous puzzle to see what puzzles it is compatible with
                                if (room.Puzzles.Count > 0)
                                {
                                    return;
                                }
                                Puzzle p = Puzzles[room.Puzzles[nextPuzzleIndex - 1].Id].GetComponent<Puzzle>();
                                room.Puzzles[nextPuzzleIndex] = new PuzzleData(p.Matches[Random.Range(0, p.Matches.Length)], cx, cy);
                                nextPuzzleIndex++;
                            }

                            if (InBounds(cx + 1, cy, room) && room.FloorTiles[cy * room.Width + (cx + 1)].Blocked == false)
                            {
                                stack.Push(new Vector2(cur.x + 1, cur.y));
                            }
                            if (InBounds(cx - 1, cy, room) && room.FloorTiles[cy * room.Width + (cx - 1)].Blocked == false)
                            {
                                stack.Push(new Vector2(cur.x - 1, cur.y));
                            }
                            if (InBounds(cx, cy + 1, room) && room.FloorTiles[(cy + 1) * room.Width + cx].Blocked == false)
                            {
                                stack.Push(new Vector2(cur.x, cur.y + 1));
                            }
                            if (InBounds(cx, cy - 1, room) && room.FloorTiles[(cy - 1) * room.Width + cx].Blocked == false)
                            {
                                stack.Push(new Vector2(cur.x, cur.y - 1));
                            }
                        }

                        break;
                    }
                }
            }
        }
    }

    private void SetBorder(RoomData room)
    {
        int topDoorChoice = room.Width / 2;
        int bottomDoorChoice = room.Width / 2;
        int wDoorChoice = room.Height / 2;
        int eDoorChoice = room.Height / 2;

        //Generate Top/Bottom Border
        for (int x = 1; x < room.Width - 1; x++)
        {
            if (x != bottomDoorChoice)
            {
                room.Walls[x] = new WallData(Random.Range(0, HorizontalWallTiles.Length), RoomGenerator.HorizontalOrientationId, x, 0);
            }
            if (x != topDoorChoice)
            {
                room.Walls[(room.Height - 1) * room.Width + x] = new WallData(Random.Range(0, HorizontalWallTiles.Length), RoomGenerator.HorizontalOrientationId, x, room.Height - 1);
            }
        }
        for (int y = 1; y < room.Height - 1; y++)
        {
            if (y != wDoorChoice)
            {
                room.Walls[y * room.Width + 0] = new WallData(Random.Range(0, WWallTiles.Length), RoomGenerator.WOrientationId, 0, y);
            }
            if (y != eDoorChoice)
            {
                room.Walls[y * room.Width + (room.Width - 1)] = new WallData(Random.Range(0, EWallTiles.Length), RoomGenerator.EOrientationId, room.Width - 1, y);
            }
        }

        //Set Corner Tile IDs
        //SE
        room.Walls[room.Width - 1] = new WallData(Random.Range(0, SECornerWallTiles.Length), RoomGenerator.SECornerOrientationId, room.Width - 1, 0);
        //SW
        room.Walls[0] = new WallData(Random.Range(0, SWCornerWallTiles.Length), RoomGenerator.SWCornerOrientationId, 0, 0);
        //NW
        room.Walls[(room.Height - 1) * room.Width] = new WallData(Random.Range(0, NWCornerWallTiles.Length), RoomGenerator.NWCornerOrientationId, 0, room.Height - 1);
        //NE
        room.Walls[(room.Height - 1) * room.Width + (room.Width - 1)] = new WallData(Random.Range(0, NECornerWallTiles.Length), RoomGenerator.NECornerOrientationId, room.Width - 1, room.Height - 1);

        //Top Door
        room.Doors[TopDoorIndex] = new DoorData(TopDoorIndex, NOrientationId, 0, 0, room);
        room.Doors[TopDoorIndex].X = topDoorChoice;
        room.Doors[TopDoorIndex].Y = room.Height - 1;

        //Left Door
        room.Doors[LeftDoorIndex] = new DoorData(LeftDoorIndex, RoomGenerator.WOrientationId, 0, 0, room);
        room.Doors[LeftDoorIndex].X = 0;
        room.Doors[LeftDoorIndex].Y = wDoorChoice;

        //Right Door
        room.Doors[RightDoorIndex] = new DoorData(RightDoorIndex, RoomGenerator.EOrientationId, 0, 0, room);
        room.Doors[RightDoorIndex].X = room.Width - 1;
        room.Doors[RightDoorIndex].Y = eDoorChoice;

        //Bottom Door
        room.Doors[BottomDoorIndex] = new DoorData(BottomDoorIndex, RoomGenerator.SOrientationId, 0, 0, room);
        room.Doors[BottomDoorIndex].X = bottomDoorChoice;
        room.Doors[BottomDoorIndex].Y = 0;
    }

    private void SetFloor(RoomData room)
    {
        for (int y = 0; y < room.Height; y++)
        {
            for (int x = 0; x < room.Width; x++)
            {
                room.FloorTiles[y * room.Width + x] = new FloorData(Random.Range(0, FloorTiles.Length), Random.Range(NOrientationId, WOrientationId + 1), x, y, false);
            }
        }
    }

    /**
        <summary>Pass int.MaxValue to accept all types</summary>
    **/
    public List<WallData> GetAdjacent(WallData cur, int typeid, RoomData room)
    {
        List<WallData> ret = new List<WallData>();

        if (cur == null)
        {
            return ret;
        }

        if (InBounds(cur.X + 1, cur.Y, room))
        {
            WallData t = room.Walls[cur.Y * room.Width + (cur.X + 1)];
            if (t != null && (t.TypeId == typeid || typeid == int.MaxValue))
            {
                ret.Add(t);
            }
        }
        if (InBounds(cur.X - 1, cur.Y, room))
        {
            WallData t = room.Walls[cur.Y * room.Width + (cur.X - 1)];
            if (t != null && (t.TypeId == typeid || typeid == int.MaxValue))
            {
                ret.Add(t);
            }
        }
        if (InBounds(cur.X, cur.Y + 1, room))
        {
            WallData t = room.Walls[(cur.Y + 1) * room.Width + cur.X];
            if (t != null && (t.TypeId == typeid || typeid == int.MaxValue))
            {
                ret.Add(t);
            }
        }
        if (InBounds(cur.X, cur.Y - 1, room))
        {
            WallData t = room.Walls[(cur.Y - 1) * room.Width + cur.X];
            if (t != null && (t.TypeId == typeid || typeid == int.MaxValue))
            {
                ret.Add(t);
            }
        }
        return ret;
    }

    private bool InBounds(int x, int y, RoomData room) // excludes the room border
    {
        return (x > 0 && x < room.Width - 1) && (y > 0 && y < room.Height - 1);
    }
}
