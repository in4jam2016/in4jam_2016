# Crazy Water #


### What is this repository for? ###

This repo is for the code that goes into the Crazy Water project. There is information in the wiki about the tools we are using to get set up.

### How do I get set up? ###

Clone the repository to your workspace, open Unity and select open project and point it to the cloned repo. Unity will generate the other files needed for the project to work. 
*MUST USE UNITY 5.4*

### Wait, what? ###

* If you are a part of the project but don't have access to the repo contact Hunter. 
* If you have any questions about how to use git, well google it. Lawl